﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIHomework.data
{
  public interface IDbContext: IDisposable
  {
    DbSet<TEntity> Set<TEntity>() where TEntity : class;

  }
}
