﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIHomework.Models;

namespace WebAPIHomework.data
{
  public class BetContext : DbContext
  {
    public DbSet<Bet> Bets{ get; set; }
    public DbSet<User> Users{ get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=LinqAPI");
    }
  }
}
