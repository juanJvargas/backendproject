﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIHomework.Models
{
    public class User
    {
        /// <summary>
        /// User identifier
        /// </summary>
        [Required]
        public Guid UserId { get; set; }
        /// <summary>
        /// User full name 
        /// </summary>
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public List<Bet> Bets { get; set; }
  }
}
