﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIHomework.Models
{
    public class Bet
    {
        /// <summary>
        /// Bet identifier
        /// </summary>
        [Required]
        public Guid BetId { get; set; }
        /// <summary>
        /// Game identifier
        /// </summary>
        [Required]
        public Guid GameId { get; set; }
        /// <summary>
        /// Gambler identifier
        /// </summary>
        [Required]
        public User User { get; set; }
        public Guid UserId { get; set; }

    /// <summary>
    /// Local team score of the game
    /// </summary>
    public string LocalScore { get; set; }
        /// <summary>
        /// Guest team score of the game
        /// </summary>
        public string GuestScore { get; set; }
        /// <summary>
        /// Alrredy played?
        /// </summary>
        public bool Completed { get; set; }


    }
}
