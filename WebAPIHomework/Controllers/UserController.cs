﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIHomework.Models;
using WebAPIHomework.helper;

namespace WebAPIHomework.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private ICollections _users;
     private static BetContext _betContext;


    public UserController(ILogger<UserController> logger, ICollections users, BetContext context)
        {
            _logger = logger;
            _users = users;
      _betContext = context;

        }
        [HttpGet]
        public IActionResult Get()
        {
        var users = _betContext.Users.ToList();
            if (_users.NonEmptyUsers()) return Ok(_users.GetAllUsers());
            else return NoContent();
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetUserById(int id)
        {
            return Ok("Un solo usuario por ID");
        }
        [HttpPost]
        public IActionResult CreateUser([FromBody]  User user)
        {
            _users.CreateUser(user);
            return Created("api/users", user);
        }

        [HttpPatch("{id}")]
        public IActionResult PartialUpdateUser(int id)
        {
            Dictionary<String, String> result = new Dictionary<string, string>();
            result.Add("result", "Partial Update");
            return Created($"api/users/{id}", result);
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceUser(int id)
        {
            Dictionary<String, String> result = new Dictionary<string, string>();
            result.Add("result", "Succesfull Replace");
            return Created($"api/users/{id}", result);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            Dictionary<String, String> result = new Dictionary<string, string>();
            result.Add("result", "Succesfull Delete");
            return Created($"api/users/{id}", result);
        }
    }
}
