﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using WebAPIHomework.data;
using WebAPIHomework.helper;
using WebAPIHomework.Models;

namespace WebAPIHomework.Controllers
{
  [ApiController]
  [Route("api/bets")]
  public class BetController : ControllerBase
  {

    private readonly ILogger<BetController> _logger;
    private ICollections _bet;
    private static BetContext _betContext;

    public BetController(ILogger<BetController> logger, ICollections bet, BetContext context)
    {
      _logger = logger;
      _bet = bet;
      _betContext = context;
    }

    [HttpGet("{id}")]
    public IActionResult GetBetById(int id)
    {
      return Ok();
    }
        /// <summary>
        /// This endpoint returns all the registered bets in the system
        /// </summary>
        /// <returns>
        /// Returns a list of bets
        /// </returns>
        [HttpGet]
        public IActionResult Get()
        {
          _betContext.Database.EnsureCreated();
          var data = _betContext.Bets.ToList();
          if (data.Count == 0)
            return StatusCode((int)HttpStatusCode.NoContent);
          else
            return Ok(data);
        }
        //[HttpGet]
        //[Route("{id}")]
        //public IActionResult GetBetById(int id)
        //{
        //    return Ok("Un solo usuario por ID");
        //}
        /// <summary>
        /// Endpoint that register a new bet 
        /// </summary>
        /// <param name="bet"></param>
        /// <returns>
        /// Returnsthe object that have benn created on the system
        /// </returns>
        [HttpPost]
        public IActionResult CreateBet([FromBody] Bet bet)
        {
            _bet.CreateBet(bet);
            return Created("api/users", bet);
        }
        /// <summary>
        /// Endpoint thet gets all gambles of one gambler
        /// </summary>
        /// <param name="id"> identifier of the user</param>
        /// <returns>a list of bets</returns>

        [HttpGet("user/{id}")]
        public IActionResult GetBetsByUserId(Guid id) 
        {
            List<Bet> bets = _bet.GetBetsByUserId(id);
            return Ok(bets);
        }

        //[HttpPatch("{id}")]
        //public IActionResult PartialUpdateBet(int id)
        //{
        //    Dictionary<String, String> result = new Dictionary<string, string>();
        //    result.Add("result", "Partial Update");
        //    return Created($"api/users/{id}", result);
        //}

        //[HttpPut("{id}")]
        //public IActionResult ReplaceBet(int id)
        //{
        //    Dictionary<String, String> result = new Dictionary<string, string>();
        //    result.Add("result", "Succesfull Replace");
        //    return Created($"api/users/{id}", result);
        //}

        //[HttpDelete("{id}")]
        //public IActionResult DeletBet(int id)
        //{
        //    Dictionary<String, String> result = new Dictionary<string, string>();
        //    result.Add("result", "Succesfull Delete");
        //    return Created($"api/users/{id}", result);
        //}
    }
}
