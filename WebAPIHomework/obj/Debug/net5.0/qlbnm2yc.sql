﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

CREATE TABLE [Users] (
    [UserId] uniqueidentifier NOT NULL,
    [Name] nvarchar(200) NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([UserId])
);
GO

CREATE TABLE [Bets] (
    [BetId] uniqueidentifier NOT NULL,
    [GameId] uniqueidentifier NOT NULL,
    [UserId] uniqueidentifier NOT NULL,
    [LocalScore] nvarchar(max) NULL,
    [GuestScore] nvarchar(max) NULL,
    [Completed] bit NOT NULL,
    CONSTRAINT [PK_Bets] PRIMARY KEY ([BetId]),
    CONSTRAINT [FK_Bets_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([UserId]) ON DELETE CASCADE
);
GO

CREATE INDEX [IX_Bets_UserId] ON [Bets] ([UserId]);
GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20210930193937_init', N'5.0.10');
GO

COMMIT;
GO

