﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIHomework.Models;

namespace WebAPIHomework.helper
{
    public interface ICollections 
    {
        /// <summary>
        /// Get all users 
        /// </summary>
        /// <returns>List of users</returns>
        public List<User> GetAllUsers();
        /// <summary>
        /// Create a new user
        /// </summary>
        /// <param name="user">User that is going to be created</param>
        public void CreateUser(User user);
        /// <summary>
        /// Checks if at least one user is registered
        /// </summary>
        /// <returns>Boolean</returns>
        bool NonEmptyUsers();


        /// <summary>
        /// Get all bets
        /// </summary>
        /// <returns> List of bets</returns>
        public List<Bet> GetAllBets();
        /// <summary>
        /// create a new bet
        /// </summary>
        /// <param name="bet">bet that is going to be created</param>
        public void CreateBet(Bet bet);
        /// <summary>
        /// checks if at least one bet is registered
        /// </summary>
        /// <returns>boolean</returns>
        bool NonEmptyBets();
        /// <summary>
        /// gets all users by id
        /// </summary>
        /// <param name="id">Identifier of the user</param>
        /// <returns>a list of bets</returns>
        List<Bet> GetBetsByUserId(Guid id);
    }
}
