﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPIHomework.Models;
namespace WebAPIHomework.helper
{
    /// <summary>
    /// Class that handles all the collections(tables) in the system
    /// </summary>
    public class Collections : ICollections
    {
        private readonly List<User> _users;
        private readonly List<Bet> _bets;
        /// <summary>
        /// Contructor of the class
        /// </summary>
        public Collections()
        {
            _users = new List<User>();
            _bets = new List<Bet>();
        }
        /// <summary>
        /// Get all registered users
        /// </summary>
        /// <returns>
        /// List of users
        /// </returns>
        public List<User> GetAllUsers()
        {
            return _users;
        }
        /// <summary>
        /// Fuctino that create and save a new user in the system
        /// </summary>
        /// <param name="user">
        /// User that is going to be created
        /// </param>
        public void CreateUser(User user)
        {
            _users.Add(user);
        }
        /// <summary>
        /// Function that check that there are any users in teh system
        /// </summary>
        /// <returns>
        /// Boollean
        /// </returns>
        public bool NonEmptyUsers()
        {
            return _users.Any();
        }
        /// <summary>
        /// Funtion that get all registered bets
        /// </summary>
        /// <returns>
        /// A list of bets
        /// </returns>
        public List<Bet> GetAllBets()
        {
            return _bets;
        }
        /// <summary>
        /// Funtion Taht create and add a bet in the system
        /// </summary>
        /// <param name="bet"> Bet that is going to be created</param>
        public void CreateBet(Bet bet)
        {
            _bets.Add(bet);
        }
        /// <summary>
        /// Function to check if there are any gamble registered in teh system
        /// </summary>
        /// <returns>
        /// Bool that indicates if there are any gambles or not
        /// </returns>
        public bool NonEmptyBets()
        {
            return _bets.Any();
        }
        /// <summary>
        /// Get all the bets made by one gambler
        /// </summary>
        /// <param name="id"> Identifier of the gambler</param>
        /// <returns>
        /// A list gambles made by teh gambler
        /// </returns>
        public List<Bet> GetBetsByUserId(Guid id) 
        {
            return _bets.Where(( (Bet bet) => bet.UserId.Equals(id))).ToList();
        }
    }
}
